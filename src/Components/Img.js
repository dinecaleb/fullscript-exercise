import React from 'react';

const Img = props =>
	<li>
		<a href={props.link}>
			<img src={props.url} alt="Unsplash Image here" />
		</a>
		<p>
			<a href={props.user}>{props.name}
			</a>
			&#9825; {props.likes}
			<span> 	{props.date}</span>
		
		</p>
	</li>;

export default Img;
